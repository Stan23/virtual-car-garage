create schema if not exists cargarage collate utf8_general_ci;

create table customer
(
    customer_id         int auto_increment
        primary key,
    username            varchar(40) null,
    password            int         null,
    customer_details_id int         null,
    constraint customer_username_uindex
        unique (username)
);

create table car
(
    car_id                    int auto_increment
        primary key,
    manufacturer              varchar(30) null,
    model                     varchar(30) null,
    year                      int         null,
    registration_plate_number varchar(30) not null,
    customer_id               int         null,
    constraint car_registration_plate_number_uindex
        unique (registration_plate_number),
    constraint FK_customer_car
        foreign key (customer_id) references customer (customer_id)
);

create table customer_details
(
    customer_id  int         not null
        primary key,
    first_name   varchar(40) null,
    last_name    varchar(40) null,
    phone_number int(15)     null,
    constraint FK_customer_customer_details
        foreign key (customer_id) references customer (customer_id)
);

create table `order`
(
    order_id     int auto_increment
        primary key,
    car_id       int         null,
    service_type varchar(30) null,
    status       varchar(20) null,
    cost         double      null,
    start_date   date        null,
    end_date     date        null,
    constraint FK_car_order
        foreign key (car_id) references car (car_id)
);